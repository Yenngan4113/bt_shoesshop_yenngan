import logo from "./logo.svg";
import "./App.css";
import { Fragment } from "react";
import BT_shoes from "./BT_shoes_Component/BT_shoes";

function App() {
  return (
    <Fragment>
      <BT_shoes />
    </Fragment>
  );
}

export default App;
