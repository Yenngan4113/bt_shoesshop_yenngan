import React, { Component } from "react";
import ShoesItem from "./ShoesItem";

export default class ShoesList extends Component {
  render() {
    return (
      <div>
        <div className="row">
          {this.props.shoesList.map((item, index) => {
            return (
              <ShoesItem
                Item={item}
                Index={index}
                handleAddItemtoCart={this.props.handleAddItemtoCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
