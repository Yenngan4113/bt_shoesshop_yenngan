import React, { Component } from "react";

export default class ShoesItem extends Component {
  render() {
    let { name } = this.props.Item;
    let { image } = this.props.Item;
    let { description } = this.props.Item;
    let { price } = this.props.Item;

    return (
      <div className="col-4" key={this.props.Index}>
        <div className="card my-3" style={{ height: "640px" }}>
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              <span>{price}$</span>
              <br />
              {description}
            </p>
            <a
              href="#"
              className="btn btn-warning"
              onClick={() => {
                this.props.handleAddItemtoCart(this.props.Item);
              }}
            >
              <b>Add to cart</b>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
