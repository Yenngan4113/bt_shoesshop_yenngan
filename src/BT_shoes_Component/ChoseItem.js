import React, { Component } from "react";

export default class ChoseItem extends Component {
  render() {
    let total = this.props.chosenItem.reduce(this.props.handleSum, 0);
    return (
      <table className="table w-100">
        <thead>
          <tr>
            <td colspan="4" className="">
              Total :
            </td>
            <td>{total}$</td>
          </tr>
          <tr>
            <td>ID</td>
            <td>Product</td>
            <td>Price/Item</td>
            <td>Quantity</td>
            <td>Total</td>
            <td>Action</td>
          </tr>
        </thead>
        <tbody>
          {this.props.chosenItem.map((item, index) => {
            return (
              <tr key={index}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.price}$</td>

                <td>
                  <button
                    className="btn btn-danger mr-1"
                    onClick={() => {
                      this.props.handleReduceItemQuantity(item);
                    }}
                  >
                    -
                  </button>
                  {item.quantity}
                  <button
                    className="btn btn-success ml-1"
                    onClick={() => {
                      this.props.handleIncreseItemQuantity(item);
                    }}
                  >
                    +
                  </button>
                </td>
                <td>{`${item.price}` * `${item.quantity}`}$</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => {
                      this.props.handleDeleteItem(item);
                    }}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}
