import React, { Component, Fragment } from "react";
import ShoesList from "./ShoesList";
import { data } from "./data";
import ChoseItem from "./ChoseItem";

export default class BT_shoes extends Component {
  state = {
    shoesList: data,
    cart: [],
  };
  handleAddItemtoCart = (item) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.id == item.id;
    });
    if (index == -1) {
      let newItem = { ...item, quantity: 1 };

      cloneCart.push(newItem);
    } else {
      cloneCart[index].quantity++;
    }

    this.setState({ cart: cloneCart });
  };
  handleDeleteItem = (item) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.id == item.id;
    });
    if (index != -1) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  handleReduceItemQuantity = (item) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.id == item.id;
    });
    if (index != -1 && cloneCart[index].quantity > 2) {
      cloneCart[index].quantity--;
    } else if (cloneCart[index].quantity == 2) {
      cloneCart[index].quantity--;
    } else {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  handleIncreseItemQuantity = (item) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.id == item.id;
    });
    if (index != -1) {
      cloneCart[index].quantity++;
    }
    this.setState({ cart: cloneCart });
  };
  handleSum = (accumulator, currentValue) => {
    return accumulator + currentValue.quantity * currentValue.price;
  };
  render() {
    return (
      <Fragment>
        <div className="row">
          <div className="col-7">
            <ShoesList
              shoesList={this.state.shoesList}
              handleAddItemtoCart={this.handleAddItemtoCart}
            />
          </div>
          <div className="col-5">
            {!!this.state.cart.length && (
              <ChoseItem
                chosenItem={this.state.cart}
                Delete={this.state.delete}
                handleReduceItemQuantity={this.handleReduceItemQuantity}
                handleDeleteItem={this.handleDeleteItem}
                handleIncreseItemQuantity={this.handleIncreseItemQuantity}
                handleSum={this.handleSum}
              />
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}
